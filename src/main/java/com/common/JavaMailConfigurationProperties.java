package com.common;

import org.springframework.context.annotation.Bean;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

@Component
public class JavaMailConfigurationProperties {

    @Bean
    public JavaMailSender javaMailSender(){
        JavaMailSenderImpl mail = new JavaMailSenderImpl();
        mail.setHost("smtp.mailtrap.io");
        mail.setPort(587);
        mail.setUsername("33a1ef177ffa37");
        mail.setPassword("eb541dc7168a07");
        return mail;
    }
}
