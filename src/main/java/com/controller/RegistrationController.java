package com.controller;

import com.service.iface.company.SimpleCompany;
import com.service.iface.registration.RegistrationServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import static com.rest.RestConstantAndUtils.BASE_PATH_URL;

@Controller
public class RegistrationController {

    private static final String REGISTRATION= "registration";
    private static final String SEND_REGISTRATION= BASE_PATH_URL + "send";
    private static final String COMPANY= "company";

    private RegistrationServices registrationServices;

    public RegistrationController(RegistrationServices registrationServices) {
        this.registrationServices = registrationServices;
    }

    @RequestMapping(value = BASE_PATH_URL)
    public String initRegistration(Model model){
        model.addAttribute(COMPANY, new SimpleCompany());
        return REGISTRATION;
    }

    @PostMapping(value = SEND_REGISTRATION+BASE_PATH_URL+REGISTRATION)
    public String sendPetitionOfNewCompany(SimpleCompany company, Model model){
        Boolean registrationSuccess = registrationServices.addNewCompanyAndSendEmail(company);
        model.addAttribute(COMPANY,company);
        return REGISTRATION;
    }
}
