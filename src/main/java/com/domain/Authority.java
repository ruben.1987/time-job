package com.domain;

public enum Authority{
    ROLE_MASTER,
    ROLE_ADMIN,
    ROLE_USER;

    public static final String ROLE_MASTER_STRING = "ROLE_MASTER";
    public static final String ROLE_ADMIN_STRING = "ROLE_ADMIN";
    public static final String ROLE_USER_STRING = "ROLE_USER";

    public String getShortForm() {
        return name().replace("ROLE_", "");
    }
}
