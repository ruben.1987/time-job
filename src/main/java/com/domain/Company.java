package com.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "company")
@NoArgsConstructor
@ApiModel(description = "Class representative the company")
public class Company implements Serializable {

    private static final Long serialVersionUID = 1L;

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "primary key of the company",position = 0)
    private Long id;

    @Column(unique = true)
    @NonNull
    @ApiModelProperty(notes = "Name of the company",example = "Time Job S.L. ",position = 1)
    private String name;

    @Column(unique = true)
    @NonNull
    @ApiModelProperty(notes = "N.I.F of the company",example = "0123456P",position = 3)
    private String nif;

    @Column(unique = true)
    @NonNull
    @ApiModelProperty(notes = "Email of the company",example = "timejob@timejob.es",position = 4)
    private String email;

    @Column
    @NonNull
    @ApiModelProperty(notes = "Address of the company",example = "C/ Nueva Nº20",position = 5)
    private String address;

    @Column
    @NonNull
    @ApiModelProperty(notes = "Country where is the headquarters",example = "Spain",position = 6)
    private String country;

    @Column
    @NonNull
    @ApiModelProperty(notes = "City where is the headquarters",example = "Spain",position = 6)
    private String city;

    @Column
    @NonNull
    @ApiModelProperty(notes = "Postal code of the city where is the headquarters",example = "Spain",position = 7)
    private Integer postalCode;

    @OneToMany(mappedBy = "company", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonIgnore
    private List<Employee> employees = new ArrayList<>();

    @Builder
    public Company(String name, String nif,String email, String address,
                   String country,String city, Integer postalCode) {
        this.name = name;
        this.nif = nif;
        this.email = email;
        this.address = address;
        this.country = country;
        this.postalCode = postalCode;
        this.city=city;
        this.employees = new ArrayList<>();
    }
}
