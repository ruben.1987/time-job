package com.domain;

public enum ContractTypes {
    COMPLETE,
    PARTIAL;

    public static final String COMPLETE_STRING="COMPLETE";
    public static final String PARTIAL_STRING ="PARTIAL";
}
