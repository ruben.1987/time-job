package com.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "employee")
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Employee implements Serializable {

    private static final Long serialVersionUID = 1L;

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @NonNull
    private String name;

    @Column
    @NonNull
    private String firstName;

    @Column
    private String lastName;

    @Column(unique = true)
    @NonNull
    private String email;

    @Column(unique = true)
    @NonNull
    private String dni;

    @Column
    @NonNull
    private String address;

    @Column
    @NonNull
    private String country;

    @Column
    @NonNull
    private String city;

    @Column
    @NonNull
    private Integer postalCode;

    @Column
    @NonNull
    private String contractType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "companyId", referencedColumnName = "id")
    @NonNull
    @JsonIgnore
    private Company company;

    @OneToOne(mappedBy = "employee", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonIgnore
    private User user;

    @Builder
    public Employee(String name, String firstName,String lastName, String email, String dni,
                    String address, String country, String city, Integer postalCode,String contractType,
                    Company company, User user) {
        this.name = name;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.dni = dni;
        this.address = address;
        this.country = country;
        this.city = city;
        this.postalCode = postalCode;
        this.contractType = contractType;
        this.company = company;
        this.user = user;
    }
}
