package com.domain;

public enum RecordTypes {
    CHECKIN,
    PAUSE,
    BACK,
    CHECKOUT;

    public static final String CHECKIN_STRING="CHECKIN";
    public static final String PAUSE_STRING ="PAUSE";
    public static final String BACK_STRING ="BACK";
    public static final String CHECKOUT_STRING ="CHECKOUT";
}
