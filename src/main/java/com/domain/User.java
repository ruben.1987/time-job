package com.domain;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "users")
@NoArgsConstructor
public class User implements Serializable {

    private static final Long serialVersionUID = 1L;

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    @NonNull
    private String username;

    @Column
    @NonNull
    private String password;

    @Column
    @NonNull
    private Boolean enabled;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employeeId", referencedColumnName = "id")
    @NonNull
    @JsonIgnore
    private Employee employee;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonManagedReference
    private List<UserAuthorities> userAuthorities = new ArrayList<>();

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JsonIgnore
    private List<Registries> registries = new ArrayList<>();

    @Builder
    public User(String username, String password, Boolean enabled, Employee employee) {
        this.username = username;
        this.password = password;
        this.enabled = enabled;
        this.employee = employee;
        this.userAuthorities = new ArrayList<>();
        this.registries = new ArrayList<>();
    }

    public static boolean isAdmin(User user){
        Long id = user.getUserAuthorities().stream().filter(u->u.getAuthority().equals(Authority.ROLE_ADMIN)).count();
        return (id>0);
    }
}
