package com.repository;

import com.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee,Long> {
    List<Employee> findByNameContainingIgnoreCase(String filter);

    List<Employee> findByCompanyId(Long id);
}
