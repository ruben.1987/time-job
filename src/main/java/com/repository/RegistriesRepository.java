package com.repository;

import com.domain.Registries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;


public interface RegistriesRepository extends JpaRepository<Registries,Long> {

    @Query(value = "SELECT record_type FROM registries WHERE user_id=:filter ORDER BY id DESC LIMIT 1",nativeQuery = true)
    String findLastRecordTypeByUserId(@Param("filter") Long id);

    @Query(value = "SELECT * FROM registries WHERE user_id=:filter ORDER BY id DESC LIMIT 1",nativeQuery = true)
    Registries findLastRegistryByUserId(@Param("filter") Long id);

    @Query(value = "SELECT * FROM registries WHERE user_id=:id AND dates BETWEEN :init AND :end ORDER BY id ASC",nativeQuery = true)
    List<Registries> findByUserIdBetweenDates(@Param("id") Long id, @Param("init") LocalDate init, @Param("end") LocalDate end);
}
