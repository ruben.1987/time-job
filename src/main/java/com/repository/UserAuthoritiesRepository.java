package com.repository;

import com.domain.UserAuthorities;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserAuthoritiesRepository extends JpaRepository<UserAuthorities,Long> {
}
