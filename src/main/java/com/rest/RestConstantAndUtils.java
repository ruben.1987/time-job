package com.rest;

public class RestConstantAndUtils {

    public static final String BASE_PATH_URL = "/";
    public static final String ADD = BASE_PATH_URL + "add";
    public static final String DELETE = BASE_PATH_URL + "delete";
    public static final String API_URL = BASE_PATH_URL + "api";
    public static final String URL_MASTER = BASE_PATH_URL + "master";
    public static final String URL_ADMIN = BASE_PATH_URL + "admin";
    public static final String URL_USER = BASE_PATH_URL + "users";
    public static final String URL_COMPANY = BASE_PATH_URL + "company";
    public static final String URL_EMPLOYEE = BASE_PATH_URL + "employee";
    public static final String URL_REGISTRIES = BASE_PATH_URL + "registries";

    private RestConstantAndUtils() {
    }
}
