package com.rest.company;

import com.domain.Company;
import com.service.iface.company.CompanyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.rest.RestConstantAndUtils.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(API_URL+URL_MASTER+ BASE_PATH_URL + "company")
@Api("")
public class CompanyRestController {

    private CompanyService companyService;

    public CompanyRestController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @PostMapping(value = "", consumes = {APPLICATION_JSON_VALUE},produces = {APPLICATION_JSON_VALUE})
    @ApiOperation("Create a new company")
    public Long create(@ApiParam("Company information for a new company to be created") @RequestBody Company company){
        return companyService.create(company);
    }

    @GetMapping(value = "/{id}",produces = {APPLICATION_JSON_VALUE})
    @ApiOperation("Returns a specific company by their identifier")
    public Company getCompany(@ApiParam("Id of the company to be obtained. cannot be empty") @PathVariable("id") Long id){
        return companyService.getOne(id);
    }

    @GetMapping(value = "",produces = {APPLICATION_JSON_VALUE})
    @ApiOperation("Returns all companies with filter")
    public List<Company> getBussines
            (@ApiParam("Filter that find to containied name of comapny") @RequestParam(value = "filter",defaultValue = "")String filter){
        return companyService.getAll(filter);
    }

    @PutMapping(value = "",consumes = {APPLICATION_JSON_VALUE},produces = {APPLICATION_JSON_VALUE})
    @ApiOperation("Update all data of company")
    public Long update(@ApiParam("Company information to update company") @RequestBody Company company){
        return companyService.update(company);
    }

    @DeleteMapping(value = "/{id}")
    @ApiOperation("Delete company by their identifier")
    public void delete(@ApiParam("Id of the company to be obtained. cannot be empty") @PathVariable("id")Long id){
        companyService.delete(id);
    }
}
