package com.rest.employee;

import com.domain.Employee;
import com.service.iface.employee.EmployeeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.rest.RestConstantAndUtils.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(API_URL)
public class EmployeeRestController {

    private EmployeeService employeeService;

    public EmployeeRestController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @PostMapping(value = URL_ADMIN+URL_EMPLOYEE,consumes = {APPLICATION_JSON_VALUE})
    public Long create(@RequestBody Employee employee){
        return employeeService.create(employee);
    }

    @GetMapping(value = URL_USER+URL_EMPLOYEE,produces = {APPLICATION_JSON_VALUE})
    public Employee getEmployee(){
        return employeeService.getOneByUser();
    }

    @GetMapping(value = URL_ADMIN+URL_EMPLOYEE+"/{id}",produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity<Employee> getEmployee(@PathVariable("id") Long id){
        return employeeService.getOneByAdmin(id);
    }

    @GetMapping(value = URL_MASTER+URL_EMPLOYEE,produces = APPLICATION_JSON_VALUE)
    public List<Employee> getAllEmpployeesByMaster(@RequestParam("filter")String filter){
        return employeeService.getAllByMaster(filter);
    }

    @GetMapping(value = URL_ADMIN+URL_EMPLOYEE,produces = APPLICATION_JSON_VALUE)
    public List<Employee> getAllEmpployeesByAdmin(){
        return employeeService.getAllByAdmin();
    }

    @PutMapping(value = URL_USER+URL_EMPLOYEE,consumes = {APPLICATION_JSON_VALUE},produces = {APPLICATION_JSON_VALUE})
    public Long update(@RequestBody Employee employee){
        return employeeService.update(employee);
    }

    @DeleteMapping(value = URL_ADMIN+ URL_EMPLOYEE+"/{id}")
    public void delete(@PathVariable("id") Long id){
         employeeService.delete(id);
    }
}
