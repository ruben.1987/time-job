package com.rest.registries;

import com.domain.Registries;
import com.service.iface.registries.RegistriesService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

import static com.rest.RestConstantAndUtils.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = API_URL)
public class RegistriesRestController {

    private RegistriesService registriesService;

    public RegistriesRestController(RegistriesService registriesService) {
        this.registriesService = registriesService;
    }

    @PostMapping(value = URL_USER+URL_REGISTRIES+"/{id}",produces = APPLICATION_JSON_VALUE)
    public Long create(@PathVariable("id")Long id,@RequestParam("latitude")Double latitude,@RequestParam("longitude")Double longitude){
        return registriesService.create(id,latitude,longitude);
    }

    @GetMapping(value = URL_USER+URL_REGISTRIES+"/{id}")
    public String getRegistriesUser(@PathVariable("id")Long id,@RequestParam("init")@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate init,
                                             @RequestParam("end")@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate end){
        return registriesService.getAllBetweenDatesUser(id,init,end);
    }

    @GetMapping(value = URL_ADMIN+URL_REGISTRIES)
    public String getRegistriesAdmin(@RequestParam("id") Long id,@RequestParam("init")@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)LocalDate init,
                                             @RequestParam("end")@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate end){
        return registriesService.getAllBetweenDatesAdmin(id,init,end);
    }

    @GetMapping(value = URL_ADMIN+URL_USER+URL_REGISTRIES,produces = APPLICATION_JSON_VALUE)
    public List<Registries> getRegistriesUserByAdmin(@RequestParam("id") Long id, @RequestParam("init")@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)LocalDate init,
                                               @RequestParam("end")@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate end){
        return registriesService.getRegistriesByAdminForPutRegustries(id,init,end);
    }

    @PutMapping(value = URL_ADMIN+URL_REGISTRIES+"/{id}",produces = APPLICATION_JSON_VALUE)
    public Registries updateRegistriesAdmin(@PathVariable("id") Long id,@RequestBody Registries registries){
        return registriesService.updateRegistriesAdmin(id,registries);
    }
}
