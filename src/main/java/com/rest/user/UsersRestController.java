package com.rest.user;

import com.domain.User;
import com.service.iface.user.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.rest.RestConstantAndUtils.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(API_URL)
public class UsersRestController {

    private UserService userService;

    public UsersRestController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = URL_USER, produces = {APPLICATION_JSON_VALUE})
    public User getUser() {
        return userService.getUser();
    }

    @GetMapping(value = URL_ADMIN + URL_USER + "/{id}", produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity<User> getUserByAdmin(@PathVariable Long id) {
        return userService.getUserByAdmin(id);
    }

    @GetMapping(value = URL_ADMIN + URL_USER, produces = {APPLICATION_JSON_VALUE})
    public List<User> getAllUsers(@RequestParam("filter") String filter) {
        return userService.getUsers(filter);
    }

    @PutMapping(value = URL_USER, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public Long updateUser(@RequestBody User user) {
        return userService.update(user);
    }

    @PatchMapping(value = URL_ADMIN + ADD + URL_USER + "/{id}", produces = APPLICATION_JSON_VALUE)
    public Long patchAddAuthorityAdmin(@PathVariable("id") Long id) {
        return userService.patchAddAuthorityAdmin(id);
    }

    @PatchMapping(value = URL_ADMIN + DELETE + URL_USER + "/{id}", produces = APPLICATION_JSON_VALUE)
    public Long patchDeleteAuthorityAdmin(@PathVariable("id") Long id) {
        return userService.patchDeleteAuthorityAdmin(id);
    }

    @PatchMapping(value = URL_MASTER + ADD + URL_USER + "/{id}", produces = APPLICATION_JSON_VALUE)
    public Long patchAddAuthorityMaster(@PathVariable("id") Long id) {
        return userService.patchAddAuthorityMaster(id);
    }

    @PatchMapping(value = URL_MASTER + DELETE + URL_USER + "/{id}", produces = APPLICATION_JSON_VALUE)
    public Long patchDeleteAuthorityMaster(@PathVariable("id") Long id) {
        return userService.patchDeleteAuthorityMaster(id);
    }

    @DeleteMapping(value = URL_ADMIN+URL_USER + "/{id}")
    public void deleteUser(@PathVariable("id") Long id) {
        userService.delete(id);
    }
}
