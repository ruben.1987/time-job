package com.security;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import java.util.List;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class CurrentUser {

    private static final Logger LOGGER = LoggerFactory.getLogger(CurrentUser.class);

    @NonNull
    private String username;

    @NonNull
    private List<String> roles;

    public static User getCurrentUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();
        return user;
    }
}
