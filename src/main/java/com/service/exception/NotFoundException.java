package com.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class NotFoundException extends RuntimeException {

    public static final String NOT_FOUND=" Not found";

    public NotFoundException(String entity,Long id) {
        super(entity+NOT_FOUND);
    }
}
