package com.service.iface.company;

import com.domain.Company;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CompanyService {

    Long create(Company company);

    Company getOne(Long id);

    List<Company> getAll(String filter);

    Long update(Company company);

    ResponseEntity delete(Long id);
}
