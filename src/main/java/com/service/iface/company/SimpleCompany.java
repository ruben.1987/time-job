package com.service.iface.company;

import lombok.*;

import javax.persistence.Column;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class SimpleCompany implements Serializable {

    @NonNull
    private String name;

    @NonNull
    private String nif;

    @NonNull
    private String email;

    @NonNull
    private String address;

    @NonNull
    private String country;

    @NonNull
    private String city;

    @NonNull
    private Integer postalCode;
}
