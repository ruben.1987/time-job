package com.service.iface.employee;

import com.domain.Employee;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface EmployeeService {

    Long create(Employee employee);

    ResponseEntity<Employee> getOneByAdmin(Long id);

    Employee getOneByUser();

    List<Employee> getAllByAdmin();

    List<Employee> getAllByMaster(String filter);

    Long update(Employee employee);

    void delete(Long id);
}
