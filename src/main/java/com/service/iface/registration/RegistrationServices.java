package com.service.iface.registration;

import com.domain.Company;
import com.service.iface.company.SimpleCompany;

public interface RegistrationServices {

    Boolean addNewCompanyAndSendEmail(SimpleCompany company);
}
