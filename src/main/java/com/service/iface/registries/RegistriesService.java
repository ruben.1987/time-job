package com.service.iface.registries;

import com.domain.Registries;

import java.time.LocalDate;
import java.util.List;

public interface RegistriesService {

    Long create(Long id,Double latitude,Double longitude);

    Registries getLast(Long id);

    List<Registries> getRegistriesByAdminForPutRegustries(Long id, LocalDate init, LocalDate end);

    String getAllBetweenDatesUser(Long id,LocalDate init, LocalDate end);

    String getAllBetweenDatesAdmin(Long id,LocalDate init,LocalDate end);

    Registries updateRegistriesAdmin(Long id,Registries registries);
}
