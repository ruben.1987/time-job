package com.service.iface.registries;

import com.domain.RecordTypes;
import lombok.*;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class RegistriesTime {

    private LocalDateTime localDateTime;

    private RecordTypes type;

    @Builder
    public RegistriesTime(LocalDateTime localDateTime, RecordTypes type) {
        this.localDateTime = localDateTime;
        this.type = type;
    }
}
