package com.service.iface.user;

import com.domain.Employee;
import com.domain.User;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UserService {

    Long create(Employee employee);

    User getUser();

    ResponseEntity<User> getUserByAdmin(Long id);

    List<User> getUsers(String filter);

    Long update(User user);

    Long patchAddAuthorityAdmin(Long id);

    Long patchDeleteAuthorityAdmin(Long id);

    Long patchAddAuthorityMaster(Long id);

    Long patchDeleteAuthorityMaster(Long id);

    void delete(Long id);
}
