package com.service.impl.company;

import com.domain.Company;
import com.repository.CompanyRepository;
import com.service.exception.NotFoundException;
import com.service.iface.company.CompanyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class CompanyServiceJpaImpl implements CompanyService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyServiceJpaImpl.class);

    private static final String COMPANY="Company";

    private CompanyRepository companyRepository;

    public CompanyServiceJpaImpl(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    @Override
    @Transactional
    public Long create(Company company) {
        return companyRepository.save(company).getId();
    }

    public Company getCompanyOrThrow(Long id){
        return companyRepository.findById(id).orElseThrow(()-> new NotFoundException(COMPANY,id));
    }

    @Override
    public Company getOne(Long id) {
        return getCompanyOrThrow(id);
    }


    @Override
    public List<Company> getAll(String filter) {
        return companyRepository.findByNameContainingIgnoreCase(filter);
    }

    @Override
    @Transactional
    public Long update(Company company) {
        Company updateCompany = getCompanyOrThrow(company.getId());
        updateCompany = Company.builder().name(company.getName())
                .nif(company.getNif())
                .email(company.getEmail())
                .address(company.getAddress())
                .city(company.getCity())
                .country(company.getCountry())
                .postalCode(company.getPostalCode())
                .build();
        updateCompany.setId(company.getId());
        return companyRepository.save(updateCompany).getId();
    }

    @Override
    @Transactional
    public ResponseEntity delete(Long id) {
        companyRepository.deleteById(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
