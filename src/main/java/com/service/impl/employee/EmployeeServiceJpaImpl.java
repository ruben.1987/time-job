package com.service.impl.employee;

import com.domain.Company;
import com.domain.Employee;
import com.domain.User;
import com.repository.EmployeeRepository;
import com.repository.UserRepository;
import com.security.CurrentUser;
import com.service.exception.NotFoundException;
import com.service.iface.employee.EmployeeService;
import com.service.iface.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class EmployeeServiceJpaImpl implements EmployeeService {

    public static final String EMPLOYEE= "Employee";

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeServiceJpaImpl.class);

    @Autowired
    private PasswordEncoder encoder;

    private EmployeeRepository employeeRepository;

    private UserRepository userRepository;

    private UserService userService;

    public EmployeeServiceJpaImpl(EmployeeRepository employeeRepository,UserRepository userRepository,UserService userService) {
        this.employeeRepository = employeeRepository;
        this.userRepository = userRepository;
        this.userService = userService;
    }

    @Override
    @Transactional
    public Long create(Employee employee) {
        employee.setCompany(getCompanyCurrentUser(getAuthUser()));
        employeeRepository.save(employee);
        return userService.create(employee);
    }

    public String encriptedPassword(String password){
        return encoder.encode(password);
    }

    @Override
    public ResponseEntity<Employee> getOneByAdmin(Long id) {
        Employee getEmployee = getEmployeeOrThrowByAdmin(id);
        if(getAuthUser().getEmployee().getCompany().getId()==getEmployee.getCompany().getId()){
            return new ResponseEntity<Employee>(getEmployee,HttpStatus.OK);
        }
        else{
            return new ResponseEntity<Employee>(HttpStatus.UNAUTHORIZED);
        }

    }

    @Override
    public Employee getOneByUser() {
        return getEmployeeOrThrow();
    }

    public Employee getEmployeeOrThrow(){
        return employeeRepository.findById(getAuthUser().getEmployee().getId()).orElseThrow(()-> new NotFoundException(EMPLOYEE,getAuthUser().getId()));
    }

    public Employee getEmployeeOrThrowByAdmin(Long id){
        return employeeRepository.findById(id).orElseThrow(()-> new NotFoundException(EMPLOYEE,id));
    }

    @Override
    public List<Employee> getAllByAdmin() {
        return employeeRepository.findByCompanyId(getAuthUser().getEmployee().getCompany().getId());
    }

    @Override
    public List<Employee> getAllByMaster(String filter) {
        return employeeRepository.findByNameContainingIgnoreCase(filter);
    }

    @Override
    @Transactional
    public Long update(Employee employee) {
        Employee updateEmployee = getEmployeeOrThrowByAdmin(employee.getId());
        LOGGER.info("Employee: {}",employee);
        if(Boolean.TRUE.equals(User.isAdmin(getAuthUser()))){
            updateEmployee.setEmail(employee.getEmail());
            updateEmployee.setDni(employee.getDni());
            updateEmployee.setAddress(employee.getAddress());
            updateEmployee.setCity(employee.getCity());
            updateEmployee.setCountry(employee.getCountry());
            updateEmployee.setPostalCode(employee.getPostalCode());
            updateEmployee.setContractType(employee.getContractType());
            updateEmployee.getUser().setUsername(employee.getEmail());
            userRepository.save(updateEmployee.getUser());
        }
        if(Boolean.FALSE.equals(User.isAdmin(getAuthUser()))){
            updateEmployee.setDni(employee.getDni());
            updateEmployee.setAddress(employee.getAddress());
            updateEmployee.setCity(employee.getCity());
            updateEmployee.setCountry(employee.getCountry());
            updateEmployee.setPostalCode(employee.getPostalCode());
        }
        return employeeRepository.save(updateEmployee).getId();
    }

    private User getAuthUser(){
        User user = userRepository.findByUsername(CurrentUser.getCurrentUser().getUsername());
        return user;
    }

    private Company getCompanyCurrentUser(User user){
        return employeeRepository.findById(user.getId()).get().getCompany();
    }

    @Override
    @Transactional
    public void delete(Long id) {
        employeeRepository.deleteById(id);
    }
}
