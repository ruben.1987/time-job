package com.service.impl.registration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.service.iface.company.SimpleCompany;
import com.service.iface.registration.RegistrationServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class RegistrationServiceJpaImpl implements RegistrationServices {

    public static final String DEFAULT_SUBJECT = "New petition for register";
    public static final String DEFAULT_HEADER_MESSAGE = "The data of new company for add is:\n\n";

    @Autowired
    private JavaMailSender sender;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public Boolean addNewCompanyAndSendEmail(SimpleCompany company) {
        Boolean send = false;
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setSubject(DEFAULT_SUBJECT);
            message.setTo(company.getEmail());
            message.setText(customMailMessage(company));
            sender.send(message);
            send = true;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return send;
    }

    public String customMailMessage(SimpleCompany company) throws JsonProcessingException {
        return DEFAULT_HEADER_MESSAGE + objectMapper.writeValueAsString(company);
    }
}
