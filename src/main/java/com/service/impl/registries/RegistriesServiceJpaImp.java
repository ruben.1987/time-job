package com.service.impl.registries;

import com.domain.ContractTypes;
import com.domain.RecordTypes;
import com.domain.Registries;
import com.domain.User;
import com.repository.RegistriesRepository;
import com.repository.UserRepository;
import com.security.CurrentUser;
import com.service.exception.NotFoundException;
import com.service.iface.registries.RegistriesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.domain.RecordTypes.*;

@Service
public class RegistriesServiceJpaImp implements RegistriesService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RegistriesServiceJpaImp.class);

    private static final String REGISTRIES = "REGISTRIES";

    private static final String USER = "USER";

    private RegistriesRepository registriesRepository;

    private UserRepository userRepository;

    public RegistriesServiceJpaImp(RegistriesRepository registriesRepository, UserRepository userRepository) {
        this.registriesRepository = registriesRepository;
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public Long create(Long id, Double latitude, Double longitude) {
        User user = getUserOrThrow(id);
        return registriesRepository.save(createNewRegistries(user, latitude, longitude)).getId();
    }

    private Registries createNewRegistries(User user, Double latitude, Double longitude) {
        return Registries.builder()
                .latitude(latitude)
                .longitude(longitude)
                .recordType(insertCorrectRecordTypeComplete(user))
                .dates(LocalDate.now())
                .times(LocalTime.now())
                .user(user)
                .build();
    }


    @Override
    public Registries getLast(Long id) {
        return registriesRepository.findLastRegistryByUserId(id);
    }

    @Override
    public List<Registries> getRegistriesByAdminForPutRegustries(Long id, LocalDate init, LocalDate end) {
        return registriesRepository.findByUserIdBetweenDates(id, init, end);
    }

    @Override
    public String getAllBetweenDatesUser(Long id,LocalDate init, LocalDate end) {
        List<Registries> registries = registriesRepository.findByUserIdBetweenDates(id, init, end);
        Long seconds = getTotalSeconds(registries);
        return getTotalTimeBetweenDatesOfUser(seconds);
    }

    @Override
    public String getAllBetweenDatesAdmin(Long id, LocalDate init, LocalDate end) {
        List<Registries> registries = registriesRepository.findByUserIdBetweenDates(id, init, end);
        Long seconds = getTotalSeconds(registries);
        return getTotalTimeBetweenDatesOfUser(seconds);
    }

    @Override
    public Registries updateRegistriesAdmin(Long id, Registries registries) {
        Registries updateRegistries = getRegistriesOrThrow(id);
        updateRegistries.setRecordType(registries.getRecordType());
        updateRegistries.setDates(registries.getDates());
        updateRegistries.setTimes(registries.getTimes());
        updateRegistries.setLatitude(registries.getLatitude());
        updateRegistries.setLongitude(registries.getLongitude());
        return registriesRepository.save(updateRegistries);
    }

    private Registries getRegistriesOrThrow(Long id) {
        return registriesRepository.findById(id).orElseThrow(() -> new NotFoundException(REGISTRIES, id));
    }

    private User getUserOrThrow(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new NotFoundException(USER, id));
    }

    private User getAuthUser() {
        return userRepository.findByUsername(CurrentUser.getCurrentUser().getUsername());
    }

    public Long getSecodsOfDayComplete(Registries checkin, Registries pause, Registries back, Registries checkout) {
        Long secondsAll = ChronoUnit.SECONDS.between(checkin.getTimes(), checkout.getTimes());
        Long secondsMiddle = ChronoUnit.SECONDS.between(pause.getTimes(), back.getTimes());
        Long seconds = secondsAll - secondsMiddle;
        return seconds;
    }

    public Long getSecodsOfDayPartial(Registries checkin, Registries checkout) {
        return ChronoUnit.SECONDS.between(checkin.getTimes(), checkout.getTimes());
    }

    private String getTotalTimeBetweenDatesOfUser(Long seconds) {
        Long hours = TimeUnit.SECONDS.toHours(seconds);
        Long minutes = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds) * 60);
        Long totalSeconds = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) * 60);
        return "TOTAL TIME = " + hours + " HOURS " + minutes + " MINUTES " + totalSeconds + " SECONDS ";
    }

    private Long getTotalSeconds(List<Registries> registries) {
        int count = 0;
        Registries checkin = null, pause = null, back = null, checkout = null;
        LocalDate checkDay = null;
        Long seconds = Long.valueOf(0);
        for (Registries rt : registries) {
            checkDay = rt.getDates();
            count++;
            if (rt.getUser().getEmployee().getContractType().equals(ContractTypes.COMPLETE_STRING)) {
                switch (rt.getRecordType().name()) {
                    case CHECKIN_STRING:
                        checkin = rt;
                        break;
                    case PAUSE_STRING:
                        pause = rt;
                        break;
                    case BACK_STRING:
                        back = rt;
                        break;
                    case CHECKOUT_STRING:
                        checkout = rt;
                        break;
                }
                LOGGER.info("Checkin: {}", checkin);
                if (count == 4) {
                    seconds += getSecodsOfDayComplete(checkin, pause, back, checkout);
                    LOGGER.info("Seconds: {}", seconds);
                    count = 0;
                }
            }
            if (rt.getUser().getEmployee().getContractType().equals(ContractTypes.PARTIAL_STRING)) {
                switch (rt.getRecordType().name()) {
                    case CHECKIN_STRING:
                        checkin = rt;
                        break;
                    case CHECKOUT_STRING:
                        checkout = rt;
                        break;
                }
                LOGGER.info("Checkin: {}", checkin);
                LOGGER.info("Checkout: {}", checkout);
                if (count == 2) {
                    seconds += getSecodsOfDayPartial(checkin, checkout);
                    LOGGER.info("Seconds: {}", seconds);
                    count = 0;
                }
            }
        }
        return seconds;
    }

    private RecordTypes insertCorrectRecordTypeComplete(User user) {
        RecordTypes recordTypes = null;
        String lastRecordTypes = registriesRepository.findLastRecordTypeByUserId(user.getId());
        if (lastRecordTypes == null) lastRecordTypes = CHECKOUT_STRING;
        if(user.getEmployee().getContractType().equals(ContractTypes.COMPLETE_STRING)){
            switch (lastRecordTypes) {
                case CHECKIN_STRING:
                    recordTypes = PAUSE;
                    break;
                case PAUSE_STRING:
                    recordTypes = BACK;
                    break;
                case BACK_STRING:
                    recordTypes = CHECKOUT;
                    break;
                case CHECKOUT_STRING:
                    recordTypes = CHECKIN;
                    break;

            }
        }

        if(user.getEmployee().getContractType().equals(ContractTypes.PARTIAL_STRING)){
            switch (lastRecordTypes) {
                case CHECKIN_STRING:
                    recordTypes = CHECKOUT;
                    break;
                case CHECKOUT_STRING:
                    recordTypes = CHECKIN;
                    break;
            }
        }

        return recordTypes;
    }
}
