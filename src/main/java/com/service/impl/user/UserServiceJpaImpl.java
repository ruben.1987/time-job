package com.service.impl.user;

import com.domain.Authority;
import com.domain.Employee;
import com.domain.User;
import com.domain.UserAuthorities;
import com.repository.UserAuthoritiesRepository;
import com.repository.UserRepository;
import com.security.CurrentUser;
import com.service.exception.NotFoundException;
import com.service.iface.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class UserServiceJpaImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceJpaImpl.class);

    private static final String USER = "User";

    private UserRepository userRepository;

    private UserAuthoritiesRepository userAuthoritiesRepository;

    @Autowired
    private PasswordEncoder encoder;

    public UserServiceJpaImpl(UserRepository userRepository, UserAuthoritiesRepository userAuthoritiesRepository) {
        this.userRepository = userRepository;
        this.userAuthoritiesRepository = userAuthoritiesRepository;
    }

    @Override
    @Transactional
    public Long create(Employee employee) {
        User user = User.builder()
                .username(employee.getEmail())
                .password(encoder.encode(employee.getName().substring(0, 1).concat(employee.getFirstName())))
                .enabled(true)
                .employee(employee)
                .build();
        userRepository.save(user);
        return addUserAuthority(user).getEmployee().getId();
    }

    private User addUserAuthority(User user) {
        UserAuthorities userAuthorities = UserAuthorities.builder().authority(Authority.ROLE_USER).user(user).build();
        userAuthoritiesRepository.save(userAuthorities);
        return userAuthorities.getUser();
    }

    private User addAdminAuthority(User user) {
        UserAuthorities userAuthorities = UserAuthorities.builder().authority(Authority.ROLE_ADMIN).user(user).build();
        userAuthoritiesRepository.save(userAuthorities);
        return userAuthorities.getUser();
    }

    private User addMasterAuthority(User user) {
        UserAuthorities userAuthorities = UserAuthorities.builder().authority(Authority.ROLE_MASTER).user(user).build();
        userAuthoritiesRepository.save(userAuthorities);
        return userAuthorities.getUser();
    }

    @Override
    public User getUser() {
        return getUserOrThrow();
    }

    @Override
    public ResponseEntity<User> getUserByAdmin(Long id) {
        User getUser = getUserOrThrowByAdmin(id);
        if(getUser.getEmployee().getCompany().getId()==getAuthUser().getEmployee().getCompany().getId()){
            return new ResponseEntity<User>(getUser, HttpStatus.OK);
        }
        else{
            return new ResponseEntity<User>(getUser, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
        }
    }

    private User getUserOrThrow() {
        return userRepository.findById(getAuthUser().getId()).orElseThrow(() -> new NotFoundException(USER,getAuthUser().getId()));
    }

    private User getUserOrThrowByAdmin(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new NotFoundException(USER,id));
    }

    @Override
    public List<User> getUsers(String filter) {
        return userRepository.findByUsernameContainingIgnoreCase(filter);
    }

    @Override
    @Transactional
    public Long update(User user) {
        User userUpdate = getUserOrThrowByAdmin(user.getId());
        if (Boolean.TRUE.equals(User.isAdmin(getAuthUser()))){
            userUpdate.setPassword(encoder.encode(user.getPassword()));
            userUpdate.setEnabled(user.getEnabled());
        }
        if (Boolean.FALSE.equals(User.isAdmin(getAuthUser()))){
            userUpdate.setPassword(encoder.encode(user.getPassword()));
        }
        return userRepository.save(userUpdate).getId();
    }

    @Override
    @Transactional
    public Long patchAddAuthorityAdmin(Long id) {
        User userPatch = getUserOrThrowByAdmin(id);
        if(Boolean.TRUE.equals(userPatch.getUserAuthorities().size()<2)){
            addAdminAuthority(userPatch);
        }
        return userPatch.getId();
    }

    @Override
    @Transactional
    public Long patchDeleteAuthorityAdmin(Long id) {
        User userPatch = getUserOrThrowByAdmin(id);
        if(Boolean.TRUE.equals(userPatch.getUserAuthorities().size()==2)){
            deleteAuthorityById(userPatch.getUserAuthorities().get(1).getId());
        }
        return userPatch.getId();
    }

    @Override
    @Transactional
    public Long patchAddAuthorityMaster(Long id) {
        User userPatch = getUserOrThrowByAdmin(id);
        if(Boolean.TRUE.equals(userPatch.getUserAuthorities().size()==2)){
            addMasterAuthority(userPatch);
        }
        return userPatch.getId();
    }

    @Override
    @Transactional
    public Long patchDeleteAuthorityMaster(Long id) {
        User userPatch = getUserOrThrowByAdmin(id);
        if(Boolean.TRUE.equals(userPatch.getUserAuthorities().size()==3)){
            deleteAuthorityById(userPatch.getUserAuthorities().get(2).getId());
        }
        return userPatch.getId();
    }


    private void deleteAuthorityById(Long id){
        UserAuthorities authorities = getAuthorityOrThrow(id);
        userAuthoritiesRepository.delete(authorities);
    }

    private UserAuthorities getAuthorityOrThrow(Long id){
        return userAuthoritiesRepository.findById(id).orElseThrow(()-> new NotFoundException("",id));
    }

    public User getAuthUser(){
        User user = userRepository.findByUsername(CurrentUser.getCurrentUser().getUsername());
        return user;
    }

    @Override
    @Transactional
    public void delete(Long id) {
        User user = getUserOrThrowByAdmin(id);
        userRepository.delete(user);
    }
}
