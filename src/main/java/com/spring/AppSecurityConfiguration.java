package com.spring;

import com.domain.Authority;
import com.security.CurrentUserSuccessHandler;
import com.security.Status401AuthenticationFairuleHandler;
import com.security.status200LogoutSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.sql.DataSource;

import static com.rest.RestConstantAndUtils.*;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class AppSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private static final String[] AUTH_WHITELIST = {
            "/v2/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            "/configuration/ui",
            "/configuration/security",
            "/swagger-ui.html",
            "/webjars/**"
    };

    private static final String LOGIN_PAGE_DEFAULT = "/login";
    private static final String USERNAME_PARAMETER = "username";
    private static final String PASS_PARAMETER = "password";
    private static final String SESSION_ID = "JSESSIONID";
    private static final String ROOTS_URL = "/**";

    @Autowired
    private DataSource dataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery("SELECT username,password,enabled FROM users WHERE username=?")
                .authoritiesByUsernameQuery("SELECT username, authority FROM user_authorities INNER JOIN users ON user_authorities.user_id = users.id WHERE users.username=?")
                .passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(restAuthenticationEntryPoint())
                .and()
                .authorizeRequests()
                .antMatchers(AUTH_WHITELIST).permitAll()
                .antMatchers(API_URL + URL_USER + ROOTS_URL).hasAnyRole(Authority.ROLE_ADMIN.getShortForm(), Authority.ROLE_USER.getShortForm())
                .antMatchers(API_URL + URL_ADMIN + ROOTS_URL).hasAnyRole(Authority.ROLE_MASTER.getShortForm(), Authority.ROLE_ADMIN.getShortForm())
                .antMatchers(API_URL + URL_MASTER + ROOTS_URL).hasRole(Authority.ROLE_MASTER.getShortForm())
                .and()
                .formLogin()
                .usernameParameter(USERNAME_PARAMETER)
                .passwordParameter(PASS_PARAMETER)
                .successHandler(restAuthenticationSuccessHandler())
                .failureHandler(restAuthenticationFairuleHandler())
                .permitAll()
                .and()
                .logout()
                .logoutSuccessHandler(restLogoutSuccessHandler())
                .deleteCookies(SESSION_ID)
                .permitAll()
                .and()
                .csrf()
                .disable()
                .httpBasic();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public AuthenticationEntryPoint restAuthenticationEntryPoint() {
        return new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED);
    }

    @Bean
    public AuthenticationSuccessHandler restAuthenticationSuccessHandler() {
        return new CurrentUserSuccessHandler();
    }

    @Bean
    public LogoutSuccessHandler restLogoutSuccessHandler() {
        return new status200LogoutSuccessHandler();
    }

    @Bean
    public AuthenticationFailureHandler restAuthenticationFairuleHandler() {
        return new Status401AuthenticationFairuleHandler();
    }
}
