CREATE TABLE company(
    id BIGINT IDENTITY PRIMARY KEY,
    name VARCHAR(100) NOT NULL UNIQUE,
    nif VARCHAR (100) NOT NULL UNIQUE,
    email VARCHAR (100) NOT NULL UNIQUE,
    address VARCHAR (200) NOT NULL,
    country VARCHAR (100) NOT NULL,
    city VARCHAR (120) NOT NULL,
    postal_code INTEGER NOT NULL
);

CREATE TABLE employee(
    id BIGINT IDENTITY PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    first_name VARCHAR (100) NOT NULL,
    last_name VARCHAR (100),
    email VARCHAR (100) NOT NULL,
    dni VARCHAR (100) NOT NULL,
    address VARCHAR (200) NOT NULL,
    country VARCHAR (100) NOT NULL,
    city VARCHAR (120) NOT NULL,
    postal_code INTEGER,
    contract_type VARCHAR (100) NOT NULL,
    company_id BIGINT NOT NULL
);

CREATE TABLE users(
    id BIGINT IDENTITY PRIMARY KEY,
    username VARCHAR(100) NOT NULL UNIQUE,
    password VARCHAR (100) NOT NULL,
    enabled BOOLEAN NOT NULL,
    employee_id BIGINT NOT NULL
);

CREATE TABLE user_authorities(
    id BIGINT IDENTITY PRIMARY KEY,
    authority VARCHAR (100) NOT NULL,
    user_id BIGINT NOT NULL
);

CREATE TABLE registries(
    id BIGINT IDENTITY PRIMARY KEY,
    record_type VARCHAR (100) NOT NULL,
    dates DATE NOT NULL,
    times TIME NOT NULL,
    latitude DOUBLE NOT NULL,
    longitude DOUBLE NOT NULL,
    user_id BIGINT NOT NULL
);

ALTER TABLE employee ADD FOREIGN KEY (company_id) REFERENCES company (id) ON DELETE CASCADE;
ALTER TABLE users ADD FOREIGN KEY (employee_id) REFERENCES employee (id) ON DELETE CASCADE;
ALTER TABLE user_authorities ADD FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE;
ALTER TABLE registries ADD FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE;