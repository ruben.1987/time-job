package com;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TimeJobApp.class)
@TestPropertySource("classpath:test.properties")
public class TimeJobAppIntegrationTest {

    @Autowired
    private ApplicationContext applicationContext;

    @Test
    public void contextLoads(){
        assertNotNull(applicationContext);
    }
}
