package com.api;

import com.TimeJobApp;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static com.api.utils.TestApiConstantsAndUtils.*;
import static org.junit.Assert.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TimeJobApp.class, webEnvironment = RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class LoginApiTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoginApiTest.class);

    @LocalServerPort
    private int port;

    private TestRestTemplate template = new TestRestTemplate();

    private ObjectMapper objectMapper = new ObjectMapper();

    public ResponseEntity<String> postEntity(String username, String password, String url){
        HttpHeaders headers = new HttpHeaders();
        headers.set(USERNAME_PARAMETER,username);
        headers.set(PASS_PARAMETER,password);
        return template.postForEntity(url,headers,String.class);
    }

    @Test
    public void givenAuthenticationSuccessHandler200(){
        ResponseEntity<String> response = postEntity(USER_B,PASSWORD,DEFAULT_ENTRY_POINT+port+ URL_LOGIN);
        assertEquals(HttpStatus.OK,response.getStatusCode());
    }

    @Test
    public void givenAuthenticationFairuleHandler401UserNotEnabled(){
        ResponseEntity<String> response =postEntity(USER_D,PASSWORD,DEFAULT_ENTRY_POINT+port+ URL_LOGIN);
        assertEquals(HttpStatus.UNAUTHORIZED,response.getStatusCode());
    }
}
