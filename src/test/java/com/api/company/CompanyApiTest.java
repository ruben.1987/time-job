package com.api.company;

import com.TimeJobApp;
import com.domain.Company;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.util.List;

import static com.api.utils.TestApiConstantsAndUtils.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TimeJobApp.class, webEnvironment = RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class CompanyApiTest {

    private static final String COMPANY = "company";

    private static final Logger LOGGER = LoggerFactory.getLogger(CompanyApiTest.class);

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setUpMockMvc() {
        mvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }

    @Test
    @Transactional
    public void createRetrieveDeleteCompany() throws Exception {
        Long id = createCompany();
        Company getCompany = retieveCompany(id);
        assertEquals(getCompany.getName(), getCreateCompany().getName());
        assertEquals(getCompany.getNif(), getCreateCompany().getNif());
        assertEquals(getCompany.getAddress(), getCreateCompany().getAddress());
        assertEquals(getCompany.getCountry(), getCreateCompany().getCountry());
        assertEquals(getCompany.getCity(), getCreateCompany().getCity());
        assertEquals(getCompany.getPostalCode(), getCreateCompany().getPostalCode());
        deleteCompany(id);
    }

    @Test
    public void getTime(){
        LocalDate date = LocalDate.of(2020,04,1);
        date = LocalDate.of(2020,04,2);
        LOGGER.info("Day: {}",date.getDayOfMonth());
    }

    @Test
    @Transactional
    public void createRetrieveUpdateDeleteCompany() throws Exception {
        Long id = createCompany();
        Company company = retieveCompany(id);
        company.setCountry("update_country");
        Long updateId = updateCompany(company);
        Company updateCompany = retieveCompany(updateId);
        assertEquals(company.getName(), updateCompany.getName());
        assertEquals(company.getCountry(), updateCompany.getCountry());
        deleteCompany(id);
    }

    @Test
    public void retrieveAllCompaniesWithFilter() throws Exception {
        List<Company> companies = retrieveAllCompanies("job");
        assertTrue(companies.size()>0);
    }

    @Test
    public void retrieveAllCompaniesEmptyFilter() throws Exception {
        List<Company> companies = retrieveAllCompanies("");
        assertTrue(companies.size()>0);
    }

    public Long createCompany() throws Exception {
        MvcResult result = mvc.perform(post(URL_COMPANY)
                .contentType(APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(getCreateCompany()))
                .with(isMaster()))
                .andExpect(status().isOk())
                .andReturn();
        Long id = objectMapper.readValue(result
                .getResponse().getContentAsString(), Long.class);
        return id;
    }

    public Company retieveCompany(Long id) throws Exception {
        MvcResult result = mvc
                .perform(get(URL_COMPANY + "/{id}", id)
                        .with(isMaster()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_VALUE))
                .andReturn();
        Company company = objectMapper
                .readValue(result.getResponse()
                        .getContentAsString(), Company.class);
        return company;
    }

    public List<Company> retrieveAllCompanies(String filter) throws Exception{
        MvcResult result = mvc.perform(get(URL_COMPANY).param(FILTER, filter)
                .with(isMaster()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_VALUE))
                .andReturn();
        List<Company> companies = objectMapper
                .readValue(result.getResponse()
                        .getContentAsString(), new TypeReference<List<Company>>() {
                });
        return companies;
    }

    public Long updateCompany(Company company) throws Exception {
        MvcResult result = mvc
                .perform(put(URL_COMPANY)
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(company))
                        .with(isMaster()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_VALUE))
                .andReturn();
        Long id = objectMapper
                .readValue(result.getResponse()
                        .getContentAsString(), Long.class);
        return id;
    }

    public void deleteCompany(Long id) throws Exception {
        mvc.perform(delete(URL_COMPANY + "/{id}", id).with(isMaster()))
                .andExpect(status().isOk());
        mvc.perform(get(URL_COMPANY + "/{id}", id).with(isMaster()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()));
    }

    public static Company getCreateCompany() {
        return Company.builder()
                .name("name_create")
                .nif("000000000C")
                .email("email@create.com")
                .address("address_create")
                .country("county_create")
                .city("city_create")
                .postalCode(25882)
                .build();
    }
}
