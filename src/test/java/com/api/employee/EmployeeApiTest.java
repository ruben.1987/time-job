package com.api.employee;

import com.TimeJobApp;
import com.domain.ContractTypes;
import com.domain.Employee;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static com.api.utils.TestApiConstantsAndUtils.*;
import static org.junit.Assert.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TimeJobApp.class, webEnvironment = RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class EmployeeApiTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeApiTest.class);

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setUpMockMvc() {
        mvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }

    @Test
    public void createRetieveDeleteEmployeeByAdmin() throws Exception {
        Long id = createEmployee();
        LOGGER.info("ID: {}",id);
        Employee employee = retrieveEmployee(id,isAdmin());
        Employee getEmployee = getCreateEmployee();
        assertEquals(employee.getName(),getEmployee.getName());
        assertEquals(employee.getCity(),getEmployee.getCity());
        deleteEmployee(id);
    }

    @Test
    public void createRetrieveUpdateDeleteEmployeeByAdmin() throws Exception {
        Long id = createEmployee();
        Employee employee = retrieveEmployee(id,isAdmin());
        employee.setEmail("update@email.com");
        Long updateId = updateEmployee(employee,isAdmin());
        Employee updateEmployee = retrieveEmployee(updateId,isAdmin());
        assertEquals(employee.getName(),updateEmployee.getName());
        assertEquals(employee.getEmail(),updateEmployee.getEmail());
        deleteEmployee(id);
    }

    @Test
    @Transactional
    public void createRetrieveUpdateDeleteEmployeeByUser() throws Exception {
        Long id = createEmployee();
        Employee employee = retrieveEmployee(id,isAdmin());
        employee.setEmail("update@email.com");
        Long updateId = updateEmployee(employee,isUser());
        Employee updateEmployee = retrieveEmployee(updateId,isAdmin());
        assertNotEquals(employee.getEmail(),updateEmployee.getEmail());
        deleteEmployee(id);
    }

    @Test
    public void retrieveAllEmployeesByMasterWithFilter() throws Exception {
        List<Employee> employees = retrieveAllEmployeeByMaster("ose");
        assertTrue(employees.size()>0);
    }

    @Test
    public void retrieveAllEmployeesByMasterEmptyFilter() throws Exception {
        List<Employee> employees = retrieveAllEmployeeByMaster("");
        assertTrue(employees.size()>0);
    }

    @Test
    public void retrieveAllEmployeesByAdminSameCompany() throws Exception {
        List<Employee> employees = retrieveAllEmployeeByAdmin();
        assertTrue(employees.size()>0);
    }


    public Long createEmployee() throws Exception {
        MvcResult result = mvc.perform(post(URL_ADMIN+URL_EMPLOYEE)
                .contentType(APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(getCreateEmployee()))
                .with(isAdmin()))
                .andExpect(status().isOk())
                .andReturn();
        Long id = objectMapper.readValue(result
                .getResponse().getContentAsString(), Long.class);
        return id;
    }

    public Employee retrieveEmployee(Long id, RequestPostProcessor user) throws Exception {
        MvcResult result = mvc
                .perform(get(URL_ADMIN+URL_EMPLOYEE + "/{id}", id)
                        .with(user))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_VALUE))
                .andReturn();
        Employee employee = objectMapper
                .readValue(result.getResponse()
                        .getContentAsString(), Employee.class);
        return employee;
    }

    public List<Employee> retrieveAllEmployeeByMaster(String filter) throws Exception {
        MvcResult result = mvc
                .perform(get(URL_MASTER+URL_EMPLOYEE)
                        .param(FILTER,filter)
                        .with(isMaster()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_VALUE))
                .andReturn();
        List<Employee> employees = objectMapper
                .readValue(result.getResponse()
                        .getContentAsString(), new TypeReference<List<Employee>>() {
                });
        return employees;
    }

    public List<Employee> retrieveAllEmployeeByAdmin() throws Exception {
        MvcResult result = mvc
                .perform(get(URL_ADMIN+URL_EMPLOYEE)
                        .with(isAdmin()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_VALUE))
                .andReturn();
        List<Employee> employees = objectMapper
                .readValue(result.getResponse()
                        .getContentAsString(), new TypeReference<List<Employee>>() {
                });
        return employees;
    }

    public Long updateEmployee(Employee employee, RequestPostProcessor user) throws Exception{
        MvcResult result = mvc.perform(put(API_URL+URL_USER+URL_EMPLOYEE)
                .contentType(APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(employee))
                .with(user))
                .andExpect(status().isOk())
                .andReturn();
        Long id = objectMapper.readValue(result
                .getResponse().getContentAsString(), Long.class);
        return id;
    }

    public void deleteEmployee(Long id) throws Exception {
        mvc.perform(delete(URL_ADMIN+URL_EMPLOYEE+"/{id}",id).with(isAdmin()))
                .andExpect(status().isOk());
        mvc.perform(get(URL_ADMIN+URL_EMPLOYEE+"/{id}",id).with(isAdmin()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()));
    }

    public static Employee getCreateEmployee() {
        return Employee.builder()
                .name("name_create")
                .firstName("first_name_create")
                .lastName("last_name_create")
                .email("email@create.com")
                .dni("dni_create")
                .address("address_create")
                .city("city_create")
                .country("country_create")
                .postalCode(00000)
                .contractType(ContractTypes.COMPLETE.name())
                .build();
    }
}
