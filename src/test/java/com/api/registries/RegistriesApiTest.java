package com.api.registries;

import com.TimeJobApp;
import com.domain.RecordTypes;
import com.domain.Registries;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import static com.api.utils.TestApiConstantsAndUtils.*;
import static org.junit.Assert.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TimeJobApp.class, webEnvironment = RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class RegistriesApiTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(RegistriesApiTest.class);

    @Autowired
    private WebApplicationContext context;

    @LocalServerPort
    private int port;

    private MockMvc mvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setUpMockMvc() {
        mvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }


    @Test
    @Transactional
    public void createCorrectOrderNewRegistriesPartial() throws Exception{
        createRegistries(Long.valueOf(4));
        createRegistries(Long.valueOf(4));
        List<Registries> registries = retrieveRegistriesByAdmin(Long.valueOf(4));
        assertEquals(registries.get(0).getRecordType(), RecordTypes.CHECKIN);
        assertEquals(registries.get(1).getRecordType(), RecordTypes.CHECKOUT);
        assertEquals(registries.get(2).getRecordType(), RecordTypes.CHECKIN);
        assertEquals(registries.get(3).getRecordType(), RecordTypes.CHECKOUT);
    }

    @Test
    @Transactional
    public void createCorrectOrderNewRegistriesComplete() throws Exception{
        createRegistries(Long.valueOf(3));
        createRegistries(Long.valueOf(3));
        createRegistries(Long.valueOf(3));
        createRegistries(Long.valueOf(3));
        List<Registries> registries = retrieveRegistriesByAdmin(Long.valueOf(3));
        assertEquals(registries.get(4).getRecordType(), RecordTypes.CHECKIN);
        assertEquals(registries.get(5).getRecordType(), RecordTypes.PAUSE);
        assertEquals(registries.get(6).getRecordType(), RecordTypes.BACK);
        assertEquals(registries.get(7).getRecordType(), RecordTypes.CHECKOUT);
    }

    @Test
    @Transactional
    public void updateRetrieveRegistries() throws Exception{
        Long id =createRegistries(Long.valueOf(3));
        LOGGER.info("ID: {}",id);
    }

    @Test
    @Transactional
    public void updateRegistries() throws Exception{
        List<Registries> registries = retrieveRegistriesByAdmin(Long.valueOf(3));
        Registries registry = registries.get(3);
        registry.setTimes(LocalTime.of(19,00,00));
        Registries updateRegistries = updateRegistriesByAdmin(registry.getId(),registry);
        assertEquals(registry.getTimes(),updateRegistries.getTimes());
    }

    @Test
    public void getTimeJobByUserBetweenDates() throws Exception{
        String time = "TOTAL TIME = " + 5 + " HOURS " + 7 + " MINUTES " + 30 + " SECONDS ";
        String getTime = retrieveTimeJobByUserBetweenDates(Long.valueOf(4));
        assertEquals(time,getTime);
        time = "TOTAL TIME = " + 31 + " HOURS " + 52 + " MINUTES " + 36 + " SECONDS ";
        getTime = retrieveTimeJobByUserBetweenDates(Long.valueOf(2));
        assertEquals(time,getTime);
    }

    @Test
    public void getTimeJobByAdminBetweenDates() throws Exception{
        String time = "TOTAL TIME = " + 5 + " HOURS " + 7 + " MINUTES " + 30 + " SECONDS ";
        String getTime = retrieveTimeJobByAdminBetweenDates(Long.valueOf(4));
        assertEquals(time,getTime);
        time = "TOTAL TIME = " + 31 + " HOURS " + 52 + " MINUTES " + 36 + " SECONDS ";
        getTime = retrieveTimeJobByAdminBetweenDates(Long.valueOf(2));
        assertEquals(time,getTime);
    }


    private Long createRegistries(Long id) throws Exception{
        MvcResult result = mvc.perform(post(API_URL+URL_USER+URL_REGISTRIES+"/{id}",id)
                .param(LATITUDE,String.valueOf(28.56398))
                .param(LONGITUDE,String.valueOf(12.56398))
                .with(isAdmin()))
                .andExpect(status().isOk())
                .andReturn();

        Long getId = objectMapper.readValue(result.getResponse().getContentAsString(),Long.class);
        return getId;
    }

    private List<Registries> retrieveRegistriesByAdmin(Long id) throws Exception{
        MvcResult result = mvc.perform(get(URL_ADMIN+URL_USER+URL_REGISTRIES)
                .param(ID_USER,String.valueOf(id))
                .param(DATE_INIT,"2020-05-12")
                .param(DATE_END,String.valueOf(LocalDate.now()))
                .with(isAdmin()))
                .andExpect(status().isOk())
                .andReturn();
        List<Registries> registries = objectMapper.readValue(result.getResponse()
                .getContentAsString(), new TypeReference<List<Registries>>() {
        });
        return registries;
    }

    private Registries updateRegistriesByAdmin(Long id,Registries registries) throws Exception{
        MvcResult result = mvc.perform(put(URL_ADMIN+URL_REGISTRIES+"/{id}",id)
                .contentType(APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(registries))
                .with(isAdmin()))
                .andExpect(status().isOk())
                .andReturn();
        Registries updateRegistries = objectMapper.readValue(result.getResponse()
                .getContentAsString(),Registries.class);
        return updateRegistries;
    }

    private String retrieveTimeJobByUserBetweenDates(Long id) throws Exception{
        MvcResult result = mvc.perform(get(API_URL+URL_USER+URL_REGISTRIES+"/{id}",id)
                .param(DATE_INIT,"2020-05-12")
                .param(DATE_END,String.valueOf(LocalDate.now()))
                .with(isUser()))
                .andExpect(status().isOk())
                .andReturn();
        String time = result.getResponse().getContentAsString();

        return time;
    }

    private String retrieveTimeJobByAdminBetweenDates(Long id) throws Exception{
        MvcResult result = mvc.perform(get(URL_ADMIN+URL_REGISTRIES)
                .param(ID_USER,String.valueOf(id))
                .param(DATE_INIT,"2020-05-12")
                .param(DATE_END, String.valueOf(LocalDate.now()))
                .with(isAdmin()))
                .andExpect(status().isOk())
                .andReturn();
        String time = result.getResponse().getContentAsString();

        return time;
    }
}
