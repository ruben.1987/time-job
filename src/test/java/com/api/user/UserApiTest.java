package com.api.user;

import com.TimeJobApp;
import com.domain.Authority;
import com.domain.User;
import com.domain.UserAuthorities;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static com.api.employee.EmployeeApiTest.getCreateEmployee;
import static com.api.utils.TestApiConstantsAndUtils.*;
import static org.junit.Assert.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TimeJobApp.class, webEnvironment = RANDOM_PORT)
@TestPropertySource("classpath:test.properties")
public class UserApiTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserApiTest.class);

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private PasswordEncoder encoder;

    private MockMvc mvc;

    private ObjectMapper objectMapper = new ObjectMapper();


    @Before
    public void setUpMockMvc() throws Exception {
        mvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
    }

    @Test
    public void retrieveDeleteUser() throws Exception {
        User user = retrieveUser(createUser());
        deleteUser(user.getId());
    }

    @Test
    public void retrieveAllUsers() throws Exception {
        List<User> users = retrieveAllUsers("e");
        assertTrue(users.size() > 0);
    }

    @Test
    public void createRetrievePutUpdateDeleteUserByAdmin() throws Exception {
        User user = retrieveUser(createUser());
        String password = user.getPassword();
        user.setPassword("password_update");
        user.setEnabled(false);
        Long updateId = putUpdateUser(user, isAdmin());
        User updateUser = retrieveUser(updateId);
        assertNotEquals(password, updateUser.getPassword());
        assertEquals(user.getEnabled(), updateUser.getEnabled());
        deleteUser(updateId);
    }

    @Test
    public void createRetrievePutUpdateDeleteUserByUser() throws Exception {
        User user = retrieveUser(createUser());
        String password = user.getPassword();
        user.setPassword("password_update");
        user.setEnabled(false);
        Long updateId = putUpdateUser(user, isUser());
        User updateUser = retrieveUser(updateId);
        assertNotEquals(password, updateUser.getPassword());
        assertNotEquals(user.getEnabled(), updateUser.getEnabled());
        deleteUser(updateId);
    }

    @Test
    public void createRetrievePatchDeleteAuthorityAdminAuthorityMasterOfUser() throws Exception {
        User user = retrieveUser(createUser());
        assertTrue(user.getUserAuthorities().size() == 1);
        Long adminAddId = pacthAddUserAuthorityAdmin(user.getId());
        User adminAddUser = retrieveUser(adminAddId);
        assertTrue(adminAddUser.getUserAuthorities().size() ==2);
        Long masterAddId = pacthAddUserAuthorityMaster(adminAddUser.getId());
        User masterAddUser = retrieveUser(masterAddId);
        LOGGER.info("UserAuthority: {}",masterAddUser.getUserAuthorities().size());
        assertTrue(masterAddUser.getUserAuthorities().size() == 3);
        Long masterdeleteId = pacthDeleteUserAuthorityMaster(masterAddUser.getId());
        User masterDeleteUser = retrieveUser(masterdeleteId);
        assertTrue(masterDeleteUser.getUserAuthorities().size() == 2);
        Long adminDeleteId = pacthDeleteUserAuthorityAdmin(masterDeleteUser.getId());
        User adminDeleteUser = retrieveUser(adminDeleteId);
        assertTrue(adminDeleteUser.getUserAuthorities().size() == 1);
        deleteUser(adminDeleteUser.getId());
    }

    public Long createUser() throws Exception {
        MvcResult result = mvc.perform(post(URL_ADMIN + URL_EMPLOYEE)
                .contentType(APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(getCreateEmployee()))
                .with(isAdmin()))
                .andExpect(status().isOk())
                .andReturn();
        Long id = objectMapper.readValue(result
                .getResponse().getContentAsString(), Long.class);

        result = mvc
                .perform(get(URL_ADMIN + URL_USER + "/{id}", id)
                        .with(isAdmin()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_VALUE))
                .andReturn();
        User user = objectMapper
                .readValue(result.getResponse()
                        .getContentAsString(), User.class);
        return user.getId();
    }

    public User retrieveUser(Long id) throws Exception {
        MvcResult result = mvc.perform(get(URL_ADMIN + URL_USER + "/{id}", id)
                .with(isAdmin()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_VALUE))
                .andReturn();
        User getUser = objectMapper
                .readValue(result.getResponse().getContentAsString(), User.class);
        return getUser;
    }

    public List<User> retrieveAllUsers(String filter) throws Exception {
        MvcResult result = mvc.perform(get(URL_ADMIN + URL_USER)
                .param(FILTER, filter)
                .with(isAdmin()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_VALUE))
                .andReturn();
        List<User> users = objectMapper
                .readValue(result.getResponse()
                        .getContentAsString(), new TypeReference<List<User>>() {
                });
        return users;
    }

    public Long putUpdateUser(User user, RequestPostProcessor processor) throws Exception {
        MvcResult result = mvc.perform(put(API_URL + URL_USER)
                .contentType(APPLICATION_JSON_VALUE)
                .content(objectMapper.writeValueAsString(user))
                .with(processor))
                .andExpect(status().isOk())
                .andReturn();
        Long id = objectMapper.readValue(
                result.getResponse().getContentAsString(), Long.class);
        return id;
    }

    public Long pacthAddUserAuthorityAdmin(Long id) throws Exception {
        MvcResult result = mvc.perform(patch(URL_ADMIN + ADD + URL_USER + "/{id}", id)
                .with(isAdmin()))
                .andExpect(status().isOk())
                .andReturn();
        Long getId = objectMapper.readValue(
                result.getResponse().getContentAsString(), Long.class);
        return getId;
    }

    public Long pacthAddUserAuthorityMaster(Long id) throws Exception {
        MvcResult result = mvc.perform(patch(URL_MASTER + ADD + URL_USER + "/{id}", id)
                .with(isMaster()))
                .andExpect(status().isOk())
                .andReturn();
        Long getId = objectMapper.readValue(
                result.getResponse().getContentAsString(), Long.class);
        return getId;
    }

    public Long pacthDeleteUserAuthorityAdmin(Long id) throws Exception {
        MvcResult result = mvc.perform(patch(URL_ADMIN + DELETE + URL_USER + "/{id}", id)
                .with(isAdmin()))
                .andExpect(status().isOk())
                .andReturn();
        Long getId = objectMapper.readValue(
                result.getResponse().getContentAsString(), Long.class);
        return getId;
    }

    public Long pacthDeleteUserAuthorityMaster(Long id) throws Exception {
        MvcResult result = mvc.perform(patch(URL_MASTER + DELETE + URL_USER + "/{id}", id)
                .with(isMaster()))
                .andExpect(status().isOk())
                .andReturn();
        Long getId = objectMapper.readValue(
                result.getResponse().getContentAsString(), Long.class);
        return getId;
    }

    public void deleteUser(Long id) throws Exception {
        mvc.perform(delete(URL_ADMIN + URL_EMPLOYEE + "/{id}", id).with(isAdmin()))
                .andExpect(status().isOk());
        mvc.perform(get(URL_ADMIN + URL_USER + "/{id}", id).with(isAdmin()))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()));
    }
}
