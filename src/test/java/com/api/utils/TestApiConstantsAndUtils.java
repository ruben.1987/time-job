package com.api.utils;


import com.domain.Authority;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.UserRequestPostProcessor;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;

public class TestApiConstantsAndUtils {

    public static final String USERNAME_PARAMETER = "username";
    public static final String PASS_PARAMETER = "password";
    public static final String USER_A = "rortega@timejob.es";
    public static final String USER_B = "mperdana@delta.com";
    public static final String USER_C = "sperez@delta.com";
    public static final String USER_D = "lrodriguez@delta.es";
    public static final String PASSWORD = "admin1234";
    public static final String DEFAULT_ENTRY_POINT = "http://localhost:";
    public static final String FILTER = "filter";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String ID_USER = "id";
    public static final String DATE_INIT= "init";
    public static final String DATE_END = "end";
    public static final String BASE_PATH_URL = "/";
    public static final String ADD = BASE_PATH_URL + "add";
    public static final String DELETE = BASE_PATH_URL + "delete";
    public static final String URL_LOGIN = BASE_PATH_URL + "login";
    public static final String URL_LOGOUT = BASE_PATH_URL + "logout";
    public static final String API_URL = BASE_PATH_URL + "api";
    public static final String URL_MASTER = API_URL + BASE_PATH_URL + "master";
    public static final String URL_ADMIN = API_URL + BASE_PATH_URL + "admin";
    public static final String URL_USER = BASE_PATH_URL + "users";
    public static final String URL_COMPANY = URL_MASTER + BASE_PATH_URL + "company";
    public static final String URL_EMPLOYEE = BASE_PATH_URL + "employee";
    public static final String URL_REGISTRIES = BASE_PATH_URL + "registries";

    public static UserRequestPostProcessor isMaster() {
        return user(USER_A).roles(Authority.ROLE_MASTER.getShortForm(), Authority.ROLE_ADMIN.getShortForm(), Authority.ROLE_USER.getShortForm());
    }

    public static UserRequestPostProcessor isAdmin() {
        return user(USER_B).roles(Authority.ROLE_ADMIN.getShortForm(), Authority.ROLE_USER.getShortForm());
    }

    public static UserRequestPostProcessor isUser() {
        return user(USER_C).roles(Authority.ROLE_USER.getShortForm());
    }
}
